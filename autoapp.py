# -*- coding: utf-8 -*-
"""Create an application instance."""
from fyrkat.app import create_app

app = create_app()
