# -*- coding: utf-8 -*-
"""Dashboard views."""
from flask import Blueprint, render_template, flash, redirect, url_for
from flask_login import login_required, current_user
from fyrkat.dashboard.forms import RatingForm
from fyrkat.ledger.models import Transaction, Account
from fyrkat.user.models import User
from fyrkat.utils import flash_errors
from fyrkat.extensions import db
from collections import defaultdict
from fyrkat.jobs.recommendations import give_recommendation

blueprint = Blueprint("dashboard", __name__, url_prefix="/dashboard", static_folder="../static")

@login_required
@blueprint.route("/rating/<transaction_id>", methods=["GET", "POST"])
def rating(transaction_id):
    """Add new rating."""
    transaction = Transaction.get_by_id(transaction_id)

    if transaction.account.user.id != current_user.id:
        flash("BACKOFF, PUNK! Not authorized.", "error")
        return redirect(url_for(".insights_new"))

    form = RatingForm(transaction)

    if form.validate_on_submit():
        transaction.base_joy_scale = form.rating.data
        transaction.save()
        flash(f"Rated ✅", "success")
        return redirect(url_for(".rate_randon_unrated"))
    else:
        flash_errors(form)
    return render_template("dashboard/rating.html", form=form, amount=round(abs(transaction.amount), 2), ben=transaction.beneficiary)

@login_required
@blueprint.route("/rating-random", methods=["GET"])
def rate_randon_unrated():
    transaction = next(iter(Transaction.uncategorised_for_user(current_user)), None)
    if not transaction:
        flash(f"Wow, you're all done, no more transactions to classify 💥", "success")
        return redirect(url_for("user.home"))
        
    return redirect(url_for('.rating', transaction_id=transaction.id))

@login_required
@blueprint.route("/insights/<year>-<month>", methods=["GET"])
def insights(year, month):
    """Insights view."""
    y = int(year)
    m = int(month)
    start = f'{year}-{m:02}-01'
    end = f'{year}-{m+1:02}-01'
    transactions = Transaction.daterange_for_user(current_user, start, end)
    uncategorized_transactions = Transaction.uncategorised_daterange_for_user(current_user, start, end)
    total_amount = round(sum(map(lambda x: abs(x.amount), transactions)), 2)
    grouped = defaultdict(list)
    for transaction in transactions: grouped[transaction.base_joy_scale].append(transaction)
    grouped_fractions = defaultdict(float)
    for k in grouped.keys(): grouped_fractions[k] = round(sum(map(lambda x: abs(x.amount), grouped[k]))/total_amount, 2)
    return render_template("dashboard/insights.html", 
            start=start, end=end, total=total_amount, 
            uncategorized_transactions=uncategorized_transactions, 
            grouped=grouped_fractions, 
            recommendations=give_recommendation(current_user.id))

@login_required
@blueprint.route("/insights/new", methods=["GET"])
def insights_new():
    latest_date = Transaction.latest_for_user(current_user).payment_date
    month = latest_date.strftime("%m")
    year = latest_date.strftime("%Y")
    return insights(year, month)
