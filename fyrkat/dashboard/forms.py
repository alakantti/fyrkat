
# -*- coding: utf-8 -*-
"""Public forms."""
from flask_wtf import FlaskForm
from wtforms import RadioField, HiddenField
from wtforms.validators import DataRequired

from fyrkat.ledger.models import Transaction


class RatingForm(FlaskForm):
    """Rating form."""

    rating = RadioField("Rating", choices=[("1", "Utility"), ("2", "Meh"), ("3", "OK"), ("4", "Great")])

    def __init__(self, transaction, *args, **kwargs):
        """Create instance."""
        super(RatingForm, self).__init__(*args, **kwargs)