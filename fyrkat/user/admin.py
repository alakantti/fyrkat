from flask_admin.contrib.sqla import ModelView
from fyrkat.extensions import admin
from fyrkat.database import db

from .models import (
    User
)

class UserAdminModelView(ModelView):
    form_excluded_columns = ('password')
    column_exclude_list = ('password')
    page_size = 200  # the number of entries to display on the list view

admin.add_view(UserAdminModelView(User, db.session, endpoint='useradmin'))