# -*- coding: utf-8 -*-
"""User views."""
from flask import Blueprint, render_template, request, flash, redirect, url_for
from flask_login import login_required, current_user
from fyrkat.database import db
from fyrkat.utils import flash_errors

from fyrkat.ledger.models import Transaction

blueprint = Blueprint("user", __name__, url_prefix="/users", static_folder="../static")


@blueprint.route("/")
@login_required
def home():
    """List members."""
    uncategorized_transactions = Transaction.uncategorised_for_user(current_user)

    if not current_user.accounts:
        return render_template("users/start.html")
    else:
        return render_template("users/home.html", uncategorized_transactions=uncategorized_transactions)

@blueprint.route("/demodata", methods=["GET", "POST"])
@login_required
def demodata():
    """Show form for adding demo data."""
    from fyrkat.jobs.intervention import InterventionCall
    from fyrkat.jobs.classification import classify_transaction_xgboost
    from fyrkat.ledger.models import Transaction
    from fyrkat.user.forms import DemoForm
    from datetime import datetime

    form = DemoForm(request.form)
    if form.validate_on_submit():
        account = current_user.accounts[0]
        # Create the transaction
        date = datetime.now()
        amount = -1.0*float(form.amount.data)
        beneficiary = form.beneficiary.data
        message = ""

        tx = Transaction(
            account=account, 
            payment_date=date,
            amount=amount,
            message=message,
            beneficiary=beneficiary
            )
        db.session.add(tx)
        db.session.commit()

        # Run classification
        tx.base_joy_scale = classify_transaction_xgboost(tx.beneficiary)
        db.session.commit()

        if (tx.base_joy_scale == 0):
            # If the added transaction was unpleasant, fire the intervention
            intervention = InterventionCall(current_user.id)
            intervention.call(beneficiary, amount)
        flash("Data added!", "success")
        return redirect(url_for("public.home"))
    else:
        flash_errors(form)
    
    return render_template("users/demodata.html", form=form)

