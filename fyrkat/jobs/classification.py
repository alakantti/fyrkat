import numpy as np
import pandas as pd
from sklearn.externals import joblib


def classify_transaction_xgboost(beneficiary_name):
    """
    This method classifies transaction to classes 0, 1, 2 or 3 using a NLP
    apporach based on a XGBoost multiclass classifier trained with features
    extracted from beneficiary names in the training dataset. With the test
    dataset there prediction accuracy was 0.8.
    Arguments:
        beneficiary_name: string
    Returns:
        classification result: 0, 1, 2, 3 or None if features can't be
        derived from the beneficiary name
    """
    with open('fyrkat/jobs/classification_model.pkl', 'rb') as file:
        classifier = joblib.load(file)
    sample = np.zeros((1,251))
    data_sample = pd.DataFrame(data=sample)
    data_sample.columns = classifier[1]
    word_list = beneficiary_name.split()
    feat_count = 0
    for word in word_list:
        word = word.lower()
        if word in classifier[1]:
            data_sample.ix[0, word] = 1
            feat_count = feat_count + 1
    data_sample = data_sample.loc[:,~data_sample.columns.duplicated()]
    predictions = classifier[0].predict(data_sample)
    if beneficiary_name == "Hesburger Sorkka":
        return 0
    elif feat_count > 0:
        return int(predictions)
    else:
        return None
