import requests
import os
from gtts import gTTS
from fyrkat.user.models import User

class InterventionCall():
    """Class to make an intervention call to user.
    """
    def __init__(self, user_id):
        # Get phone number from User account
        user = User.get_by_id(user_id)
        if (user.phone_number):
            self.phonenum = user.phone_number
            # Create caller object
            self.caller = ElksCaller(self.phonenum)
        else:
            self.caller = None
    
    def call(self, beneficiary, amount):
        if (self.caller):
            amount = abs(amount)
            # Create mp3 file from product and amount
            tts = gTTS(text='Hello. You have to be kidding me. You spent ' + str(int(amount)) + ' euros and ' + str(int(amount*100%100)) + ' cents in ' + beneficiary + '. Please stop. Goodbye.', lang='en')
            mp3file = 'static/mp3/call.mp3'
            tts.save('fyrkat/'+mp3file)

            if os.environ['FLASK_ENV'] == 'development':
                print("You are in development mode. Calling to " + str(self.phonenum) + " can not be done. The audio file is anyway stored in " + mp3file)
                #os.system("scp demo.mp3 vider@lakka.kapsi.fi:~/sites/vider.kapsi.fi/www")
                #mp3path = 'https://vider.kapsi.fi/demo.mp3'
                #self.caller.call(mp3path)
            else:
                mp3path = 'https://fyrkat.alakantti.in/'+mp3file
                # Call the user
                self.caller.call(mp3path)


class ElksCaller():
    elks_auth = (
        os.getenv('ELKS_API_USER'),
        os.getenv('ELKS_API_PASS')
        )
    def __init__(self,phonenum):
        self.phonenum = phonenum

    def call(self, mp3file):
        fields = {
            'from': '+358503579188',
            'to': self.phonenum,
            'voice_start': '{"play":"'+mp3file+'"}'
            }
        response = requests.post(
            "https://api.46elks.com/a1/calls",
            data=fields,
            auth=self.elks_auth
            )
        print(response.text)
