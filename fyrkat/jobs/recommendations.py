
def give_recommendation(user_id):
    from fyrkat.ledger.models import Account, Transaction
    from fyrkat.user.models import User

    user = User.get_by_id(user_id)
    account = user.accounts[0]
    txs = account.transactions

    # counts
    meh_count = 0
    necessity_count = 0
    ok_count = 0
    love_count = 0
    loveable_stuff = []
    for tx in txs:
        if tx.base_joy_scale == 0:
            meh_count += 1
        elif tx.base_joy_scale == 1:
            necessity_count += 1
        elif tx.base_joy_scale == 2:
            ok_count += 1
        elif tx.base_joy_scale == 3:
            love_count += 1
            loveable_stuff.append(tx.beneficiary)

    # metrics
    total_count = len(txs)
    meh_ratio = meh_count / total_count
    necessity_ratio = necessity_count / total_count
    ok_ratio = ok_count / total_count
    love_ratio = love_count / total_count

    # recommendations
    recommendations = []
    if meh_count < 2:
        recommendations.append("It seems that things are going pretty well for you! Maybe consider saving or investing?")
    elif meh_ratio < 0.1:
        recommendations.append("You have not bought much irrelevant stuff! Treat yourself! Spend more money on enjoyable things next time too!")
    if necessity_ratio < 0.2:
        recommendations.append("You might be running out of utilities, consider restocking!")
    if love_ratio > 0.2:
        if len(loveable_stuff) > 1:
            recommendations.append("Great! Keep up the good work - seems that these things make you happy, e.g., " + loveable_stuff[0] + " and " + loveable_stuff[1] + "!")
        else:
            recommendations.append("Great! Keep up the good work - seems that these things make you happy, e.g., " + loveable_stuff[0] + "!")

    return recommendations
