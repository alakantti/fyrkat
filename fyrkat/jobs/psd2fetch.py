from fyrkat.psd2clients.op import OP_API
from fyrkat.ledger.models import Account, Transaction
from fyrkat.user.models import User
from fyrkat.database import db
from fyrkat.jobs.classification import classify_transaction_xgboost
from decimal import *
import dateutil.parser

# Using the default client token for now
CLIENT_TOKEN = "7a66629ddf3691a66eb6466ab7a9f610de531047"

class PSD2Fetch():
    """Class to fetch PSD2 data for a user.
    """
    def __init__(self):
        self.api = OP_API(CLIENT_TOKEN)

    def get_accounts(self): 
       """Gets the accounts of the user as a cleaned up list.
       It's assumed that the user provides back the account IDs of
       the selected accounts in the import_transactions_method."""
       accounts = self.api.get_accounts()["accounts"]
       keys = {"accountId", "identifier", "name"}
       filtered_accounts = list(map(lambda x: {k:v for k, v in x.items() if k in keys}, accounts))
       return filtered_accounts

    def import_transactions(self, account_ext_id, user_id): 
        """Imports the transactions to SQL database from the selected account."""
        existing_account = Account.query.filter_by(external_id=account_ext_id)
        #TODO Update the new transactions from PSD2 data into existing account

        total_imported = 0
        transactions_categorized = 0

        if len(existing_account.all()) == 0:
            account_details = self.api.get_account(account_ext_id)
            user = User.get_by_id(user_id)
            account = Account(
                account_name=account_details["name"],
                external_id=account_ext_id,
                user=user
                )
            db.session.add(account)
            db.session.commit()

            transactions = self.api.get_transactions(account_ext_id)
            for transaction in transactions['transactions']:

                date = dateutil.parser.parse(transaction['valueDateTime'])
                amount = Decimal(transaction['amount'])
                if 'message' in transaction:
                    message = transaction['message']
                else:
                    message = ''

                beneficiary = transaction['creditor']['accountName']
                tx = Transaction(
                    account=account, 
                    payment_date=date,
                    amount=amount,
                    message=message,
                    beneficiary=beneficiary
                    )
                db.session.add(tx)
                # Run classification
                category = classify_transaction_xgboost(tx.beneficiary)
                total_imported += 1
                
                if category:
                    transactions_categorized += 1
                    tx.base_joy_scale = category

            db.session.commit()
            return [total_imported, transactions_categorized]
