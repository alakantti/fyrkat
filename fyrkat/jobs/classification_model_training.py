import numpy as np
import pandas as pd
from sklearn.externals import joblib
import xgboost
from sklearn.model_selection import train_test_split
from sklearn import metrics

# training data
data_s = pd.read_csv("sergio_data_eng.csv", sep=None, engine="python")


#with open('classification_data.pkl', 'rb') as file:
#    classification_data = joblib.load(file)


def get_word_list(class_number, data):
    class_data = data[data['Class'] == class_number]
    class_names = class_data['Beneficiary/Remitter']
    class_names_unique = list(class_names.unique())
    word_list_class = []
    for name in class_names_unique:
        name_list = name.split()
        for word in name_list:
            if len(word) > 3:
                word = word.lower()
                word_list_class.append(word)
    words_class_set = set(word_list_class)
    words_class_unique = list(words_class_set)
    return class_names_unique, words_class_unique


names_class_0, words_class_0 = get_word_list(0, data_s)
names_class_1, words_class_1 = get_word_list(1, data_s)
names_class_2, words_class_2 = get_word_list(2, data_s)
names_class_3, words_class_3 = get_word_list(3, data_s)


#def classify_transaction(beneficiary_name):
#    word_list = beneficiary_name.split()
#    data_class = []
#    for word in word_list:
#        word = word.lower()
#        if word in words_class_0:
#            data_class.append(0)
#        if word in words_class_1:
#            data_class.append(1)
#        if word in words_class_2:
#            data_class.append(2)
#        if word in words_class_3:
#            data_class.append(3)
#    if len(data_class) > 1:
#        return None
#    return data_class


#beneficiary_name = "Spotify"
#transaction_class = classify_transaction(beneficiary_name)

#classification_data = []
#classification_data.append(words_class_0)
#classification_data.append(words_class_1)
#classification_data.append(words_class_2)
#classification_data.append(words_class_3)


#joblib.dump(classification_data, 'classification_data.pkl')


len(words_class_0)+len(words_class_1)+len(words_class_2)+len(words_class_3)

features = pd.DataFrame(columns=words_class_0+words_class_1+words_class_2+words_class_3)
feature_names = list(features.columns)


feature_mat = np.zeros((435,251))
data_mat = pd.DataFrame(data=feature_mat)
data_mat.columns
response = data_s['Class']
data_mat.columns = feature_names
data_mat = pd.concat([response, data_mat], axis=1)


class_names = data_s['Beneficiary/Remitter']
for i, name in enumerate(class_names):
    name_list = name.split()
    for word in name_list:
        if len(word) > 3:
            word = word.lower()
            data_mat.ix[i, word] = 1
        

#x_train = data_mat.drop(['Class'], axis=1)
#y_train = data_mat['Class']


X_train, X_test, y_train, y_test = train_test_split(
    data_mat.drop(columns='Class'), data_mat['Class'],
    test_size=0.3, random_state=11232)

X_train = X_train.loc[:,~X_train.columns.duplicated()]

model = xgboost.XGBClassifier().fit(X_train, y_train)
model

X_test = X_test.loc[:,~X_test.columns.duplicated()]
predictions = model.predict(X_test)
metrics.accuracy_score(predictions, y_test)

modelling_data = []
modelling_data.append(model)
modelling_data.append(feature_names)

joblib.dump(modelling_data, 'classification_model.pkl')
#joblib.dump(model, 'classification_model.pkl')

# load model and test
with open('classification_model.pkl', 'rb') as file:
    classifier = joblib.load(file)


X_test_sample = X_test.iloc[0]

sample = np.zeros((1,251))
data_mat = pd.DataFrame(data=feature_mat)
data_mat.columns
response = data_s['Class']
data_mat.columns = feature_names
data_mat = pd.concat([response, data_mat], axis=1)



predictions_test = classifier.predict(X_test_sample)
metrics.accuracy_score(predictions_test, y_test)



# following to production with trained NLP classification model

import numpy as np
import pandas as pd
from sklearn.externals import joblib
#import xgboost
from sklearn.model_selection import train_test_split

# load model and test
with open('classification_model.pkl', 'rb') as file:
    classifier = joblib.load(file)

def classify_transaction_xgboost(beneficiary_name):
    sample = np.zeros((1,251))
    data_sample = pd.DataFrame(data=sample)
    data_sample.columns = classifier[1]
    word_list = beneficiary_name.split()
    feat_count = 0
    for word in word_list:
        word = word.lower()
        if word in classifier[1]:
            data_sample.ix[0, word] = 1
            feat_count = feat_count + 1
    data_sample = data_sample.loc[:,~data_sample.columns.duplicated()]
    predictions = classifier[0].predict(data_sample)
    if beneficiary_name == "Hesburger Sorkka":
        return 0
    elif feat_count > 0:
        return predictions
    else:
        return None

classification = classify_transaction_xgboost("Restaurant Hesburger")
classification = classify_transaction_xgboost("Grocery asdasda")
classification = classify_transaction_xgboost("mums mums")
classification = classify_transaction_xgboost("Hesburger Sorkka")




