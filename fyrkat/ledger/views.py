# -*- coding: utf-8 -*-
"""User views."""
from flask import Blueprint, render_template, request, flash, redirect, url_for
from flask_login import login_required, current_user
from fyrkat.ledger.models import Account, Transaction
from fyrkat.jobs.psd2fetch import PSD2Fetch
from .forms import AccountForm, PSDAccountForm
from .models import Account
from fyrkat.extensions import db

blueprint = Blueprint("ledger", __name__, url_prefix="/ledger", static_folder="../static")

@blueprint.route("/create-account", methods=["GET", "POST"])
@login_required
def create_account():
    account = Account()
    success = False

    if request.method == 'POST':
        form = AccountForm(request.form)
        if form.validate():
            account = Account(account_name=form.data["account_name"], user=current_user)
            db.session.add(account)
            db.session.commit()
            success = True
            return redirect(url_for("user.home"))
        else:
            flash_errors(form)
            
    form = AccountForm(obj=account)
    return render_template("ledger/create-account.html", form=form)


@blueprint.route("/import-from-psd", methods=["GET", "POST"])
@login_required
def import_from_psd():

    if request.method == 'POST':
        form = PSDAccountForm(request.form)

        # TODO handle if this is just one string, or a list of strings
        accounts_to_import = [request.form["accounts_to_import"]]

        total_categorized = 0
        total_imported = 0

        for account_to_import in accounts_to_import:

            [imported, categorized] = PSD2Fetch().import_transactions(account_to_import, current_user.id)
            total_categorized += categorized
            total_imported += imported

        flash(f"Imported {total_imported} transactions from PSD2 api 📦", "success")
        flash(f"Used Machine Learning magic to categorize {total_categorized} transactions 🧙‍♂️", "success")

        return redirect(url_for("user.home"))
    else: 
        form = PSDAccountForm()
        accounts = PSD2Fetch().get_accounts()
        choices = []

        for account in accounts:
            choices += [(account["accountId"], account["name"])]

        form.accounts_to_import.choices = choices

        return render_template("ledger/psd-create-account.html", accounts=accounts, form=form)
