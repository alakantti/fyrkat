
# -*- coding: utf-8 -*-
"""Public forms."""
from flask_wtf import FlaskForm
from wtforms import RadioField, HiddenField, StringField, SelectMultipleField, FileField
from wtforms.widgets import ListWidget, CheckboxInput
from wtforms.validators import DataRequired, InputRequired

class MultiCheckboxField(SelectMultipleField):
	widget			= ListWidget(prefix_label=False)
	option_widget	= CheckboxInput()

class AccountForm(FlaskForm):
    """Rating form."""
    account_name = StringField("Account Name")


class PSDAccountForm(FlaskForm):
    accounts_to_import = MultiCheckboxField('Accounts to import', choices=[])