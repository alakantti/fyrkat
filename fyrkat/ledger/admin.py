from flask_admin.contrib.sqla import ModelView
from fyrkat.extensions import admin
from fyrkat.database import db

from .models import (
    Account, 
    Transaction
)

class LedgerModelView(ModelView):
    page_size = 200  # the number of entries to display on the list view

admin.add_view(LedgerModelView(Account, db.session))
admin.add_view(LedgerModelView(Transaction, db.session))