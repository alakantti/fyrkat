# -*- coding: utf-8 -*-
"""Ledger models."""
import datetime as dt
import enum

from fyrkat.user.models import User

from fyrkat.database import (
    Column,
    Model,
    SurrogatePK,
    db,
    reference_col,
    relationship,
)
from sqlalchemy_utils import CurrencyType, Currency
from sqlalchemy import desc


class TransactionCategoryType(enum.Enum):

    TRANSPORTATION = "TRANSPORTATION"
    DINING = "DINING"
    LUNCH = "LUNCH"
    FAMILY = "FAMILY"
    HOME = "HOME"
    SAVINGS = "SAVINGS"
    BOATING = "BOATING"
    SUMMER_HOUSE = "SUMMER HOUSE"
    TRAVELLING = "TRAVELLING"
    SPORTS = "SPORTS"
    GAMING = "GAMING"
    CLOTHES = "CLOTHES"
    GROCERIES = "GROCERIES"
    MUSIC = "MUSIC"
    GADGETS = "GADGETS"
    LOVE = "LOVE"
    PARTY = "PARTY"

class Account(SurrogatePK, Model):

    __tablename__ = "accounts"

    account_name = Column(db.String(500))

    user_id = Column(db.Integer, db.ForeignKey('users.id'))
    user = relationship("User", backref="accounts")

    # eg. IBAN
    account_reference = Column(db.String(50))

    # eg. OPI
    external_entity = Column(db.String(50))
    external_id = Column(db.String(500), unique=True, nullable=True)

    def __repr__(self):
        """Represent instance as a unique string."""
        return f"Account {self.id}"

class Transaction(SurrogatePK, Model):

    __tablename__ = "transactions"

    account_id = Column(db.Integer, db.ForeignKey('accounts.id'))
    account = relationship("Account", backref="transactions")

    external_entity = Column(db.String(50))
    external_id = Column(db.String(500), unique=True, nullable=True)

    payment_date = Column(db.DateTime, nullable=False, default=dt.datetime.utcnow)

    beneficiary = Column(db.String(500))
    message = Column(db.String(500))
    amount = db.Column(db.Numeric)

    # Recommendation engine
    base_joy_scale = Column(db.Integer, nullable=True)
    transaction_category = db.Column(db.Enum(TransactionCategoryType))


    @classmethod
    def uncategorised_for_user(kls, user):
        return Transaction.query.join(Account).join(User).filter(User.id == user.id).filter(Transaction.base_joy_scale == None).all()

    @classmethod
    def daterange_for_user(kls, user, start, end):
        return Transaction.query.join(Account).join(User).filter(User.id == user.id).filter(Transaction.payment_date.between(start, end)).all()

    @classmethod
    def uncategorised_daterange_for_user(kls, user, start, end):
        return Transaction.query.join(Account).join(User).filter(User.id == user.id).filter(Transaction.payment_date.between(start, end)).filter(Transaction.base_joy_scale == None).all()

    @classmethod
    def latest_for_user(kls, user):
        return Transaction.query.join(Account).join(User).filter(User.id == user.id).order_by(desc(Transaction.payment_date)).first()
