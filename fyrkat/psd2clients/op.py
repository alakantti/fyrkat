import os
import requests
from time import strftime, gmtime
from uplink import Consumer, get, Path, Query, returns, response_handler, headers

if os.getenv('NDEAPY_DEBUG'):
    import logging

    # These two lines enable debugging at httplib level (requests->urllib3->http.client)
    # You will see the REQUEST, including HEADERS and DATA, and RESPONSE with HEADERS but without DATA.
    # The only thing missing will be the response.body which is not logged.
    try:
        import http.client as http_client
    except ImportError:
        # Python 2
        import httplib as http_client
    http_client.HTTPConnection.debuglevel = 1

    # You must initialize logging, otherwise you'll not see debug output.
    logging.basicConfig()
    logging.getLogger().setLevel(logging.DEBUG)
    requests_log = logging.getLogger("requests.packages.urllib3")
    requests_log.setLevel(logging.DEBUG)
    requests_log.propagate = True

# You can define these in the os level, or durint client initialization
API_KEY = os.getenv('OP_API_KEY')
CLIENT_TOKEN = os.getenv('OP_CLIENT_TOKEN')

class UnsuccessfulOPRequest(Exception):
    pass


class OP_APIConfigError(Exception):
    pass

def raise_for_status(response):
    """Checks whether or not the response was successful."""
    if 200 <= response.status_code < 300:
        # Pass through the response.
        return response

    raise UnsuccessfulOPRequest(response.url + " :: " + response.text)

@response_handler(raise_for_status)  # First, check success
@headers({})
class OP_API(Consumer):
    """A Python Client for the OP API."""

    def __init__(self, client_token=None, api_key=None):
        session = requests.Session()

        super(OP_API, self).__init__(
            base_url="https://sandbox.apis.op-palvelut.fi/accounts/v3/",
            client=session
        )
        
        self.session.headers["Authorization"] = "Bearer {}".format(str(client_token or CLIENT_TOKEN))
        self.session.headers["x-api-key"] = api_key or API_KEY
        
    @returns.json
    @get("accounts")
    def get_accounts(self): 
       """Get accounts"""

    @returns.json
    @get("accounts/{account_id}")
    def get_account(self, account_id):
       """Get account details"""

    @returns.json
    @get("accounts/{account_id}/transactions")
    def get_transactions(self, account_id):
       """Get account transactions"""
