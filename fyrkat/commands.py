# -*- coding: utf-8 -*-
"""Click commands."""
import os
import csv
from decimal import *
from datetime import datetime
from glob import glob
from subprocess import call
from flask.cli import with_appcontext
from flask import Flask
from fyrkat.database import db
from flask_collect import Collect
import click

HERE = os.path.abspath(os.path.dirname(__file__))
PROJECT_ROOT = os.path.join(HERE, os.pardir)
TEST_PATH = os.path.join(PROJECT_ROOT, "tests")

app = Flask(__name__)

@click.command()
def test():
    """Run the tests."""
    import pytest

    rv = pytest.main([TEST_PATH, "--verbose"])
    exit(rv)

@click.command()
def collect():
    collect = Collect()
    collect.collect(verbose=True)

@click.command()
@click.argument('account_id')
@click.argument('filename', type=click.Path(exists=True))
@with_appcontext
def load_test_data(account_id, filename):
    from fyrkat.ledger.models import Account, Transaction

    click.echo(f"Importing data from file {filename} to {account_id}")
    account = Account.get_by_id(account_id)

    if not account:
        click.echo(f"Account not found!", err=False)
    
    with open(filename, newline='') as csvfile:
        reader = csv.reader(csvfile, delimiter=';', quotechar='"')
        next(reader, None)  # skip the headers
        for row in reader:
            # Entry date Amount	Beneficiary/Remitter Message
            date = datetime.strptime(row[0], '%d.%M.%Y').date()
            amount = Decimal(row[1].replace(',','.'))
            beneficiary = row[2]
            message = row[3]

            if len(row) == 5:
                base_joy_scale = row[4]
            
            tx = Transaction(
                account=account, 
                payment_date=date,
                amount=amount,
                message=message,
                beneficiary=beneficiary,
                base_joy_scale=base_joy_scale
                )
            db.session.add(tx)
    db.session.commit()


@click.command()
@click.argument('user_id')
@with_appcontext
def get_from_psd(user_id):
    from fyrkat.ledger.models import Account, Transaction
    from fyrkat.jobs.psd2fetch import PSD2Fetch

    # Get all accounts from bank. Intend to provide these to user to select which ones she likes to use.
    accounts = PSD2Fetch().get_accounts()

    # Import the transactions from the selected one account
    selected_account = accounts[0]["accountId"]
    PSD2Fetch().import_transactions(selected_account,user_id)

@click.command()
@click.argument('user_id')
@click.argument('beneficiary')
@click.argument('amount')
@with_appcontext
def intervention_demo(user_id, beneficiary, amount):
    from fyrkat.ledger.models import Account, Transaction
    from fyrkat.jobs.intervention import InterventionCall
    from fyrkat.jobs.classification import classify_transaction_xgboost
    from fyrkat.user.models import User

    user = User.get_by_id(user_id)
    account = user.accounts[0]

    # Create the transaction
    date = datetime.now()
    amount = -1.0*float(amount)
    beneficiary = beneficiary
    message = ""

    tx = Transaction(
        account=account, 
        payment_date=date,
        amount=amount,
        message=message,
        beneficiary=beneficiary
        )
    db.session.add(tx)
    db.session.commit()

    # Run classification
    tx.base_joy_scale = classify_transaction_xgboost(tx.beneficiary)
    db.session.commit()

    if (tx.base_joy_scale == 0):
        # If the added transaction was unpleasant, fire the intervention
        intervention = InterventionCall(user_id)
        intervention.call(beneficiary, amount)

@click.command()
@click.argument('user_id')
@with_appcontext
def give_recommendation(user_id):
    from fyrkat.jobs.recommendations import give_recommendation
    print(give_recommendation(user_id))

@click.command()
@click.option(
    "-f",
    "--fix-imports",
    default=True,
    is_flag=True,
    help="Fix imports using isort, before linting",
)
@click.option(
    "-c",
    "--check",
    default=False,
    is_flag=True,
    help="Don't make any changes to files, just confirm they are formatted correctly",
)
def lint(fix_imports, check):
    """Lint and check code style with black, flake8 and isort."""
    skip = ["node_modules", "requirements", "migrations"]
    root_files = glob("*.py")
    root_directories = [
        name for name in next(os.walk("."))[1] if not name.startswith(".")
    ]
    files_and_directories = [
        arg for arg in root_files + root_directories if arg not in skip
    ]

    def execute_tool(description, *args):
        """Execute a checking tool with its arguments."""
        command_line = list(args) + files_and_directories
        click.echo(f"{description}: {' '.join(command_line)}")
        rv = call(command_line)
        if rv != 0:
            exit(rv)

    isort_args = ["-rc"]
    black_args = []
    if check:
        isort_args.append("-c")
        black_args.append("--check")
    if fix_imports:
        execute_tool("Fixing import order", "isort", *isort_args)
    execute_tool("Formatting style", "black", *black_args)
    execute_tool("Checking code style", "flake8")
